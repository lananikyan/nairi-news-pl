## Prerequisites

This Edition uses [Node](https://nodejs.org) for core processing, [npm](https://www.npmjs.com/) to manage project dependencies, and [gulp.js](http://gulpjs.com/) to run tasks and interface with the core library. You can follow the directions for [installing Node](https://nodejs.org/en/download/) on the Node website if you haven't done so already. Installation of Node will include npm.

## Installing

Pattern Lab Node can be used different ways. Editions like this one are **example** pairings of Pattern Lab code and do not always have an upgrade path or simple means to run as a dependency within a larger project. Users wishing to be most current and have the greatest flexibility are encouraged to consume `patternlab-node` directly. Users wanting to learn more about Pattern Lab and have a tailored default experience are encouraged to start with an Edition. Both methods still expect to interact with other elements of the [Pattern Lab Ecosystem](https://github.com/pattern-lab/patternlab-node#ecosystem).

As an Edition, the simplist installation sequence is to clone this repository.

``` bash
npm install
```

## Getting Started

This edition comes pre-packaged with a couple simple gulp tasks. Extend them as needed.

**build** patterns, copy assets, and construct ui

``` bash
gulp patternlab:build
```

build patterns, copy assets, and construct ui, watch source files, and **serve** locally

``` bash
gulp patternlab:serve
```

logs Pattern Lab Node usage and **help** content

``` bash
gulp patternlab:help
```