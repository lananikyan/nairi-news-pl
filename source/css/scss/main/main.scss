$color_1: #141414;
$color_2: $primary-color;
$color_3: #FFFFFF;
$color_4: #979797;
$color_5: #2C2C2C;
$color_6: #E0E0E0;
$color_7: #009C33;
$color_8: #FAFAFA;
$color_9: #F1F1F1;
$color_10: #626262;
$color_11: transparent;
$color_12: #494949;
$font_family_1: 'Open Sans', sans-serif;
$font_family_2: "FontAwesome";
$font_family_3: 'Open Sans Condensed', sans-serif;

.white-color a{
	color: $color_3;
}
.mod-pancakes {
	display: grid;
	grid-auto-flow: dense;
	grid-auto-rows: min-content;
	grid-gap: 18px 12px;
	grid-template-columns: 1fr;
	>* {
		margin: 0 12px;
		overflow: hidden;
	}
}
.section-head {
	h1 {
		font-size: 34px;
		color: $color_1;
		padding-bottom: 6px;
		text-transform: uppercase;
	}
}
.tag-head {
	h1 {
		font-size: 34px;
		color: $color_1;
		overflow: hidden;
		padding-bottom: 6px;
		text-transform: uppercase;
	}
}
.topic-head {
	h1 {
		font-size: 34px;
		color: $color_1;
		overflow: hidden;
		padding-bottom: 6px;
		text-transform: uppercase;
	}
}
.pancake {
	display: grid;
	grid-auto-flow: dense;
	grid-auto-rows: min-content;
	grid-gap: 12px;
	grid-template-columns: 1fr;
}
.teaser-prominent {
	background: $color_5;
	.field-name-node-title > a {
		color: $color_3;
	}
	.field-name-node-title > a {
	    color: $color_3;
	}
	.field-name-field-subtitle > a{
		color: $color_4;
		font-size: 18px;
	    font-weight: 400;
	}
}
iframe {
  max-width: 100%;
}
.teaser {
	font-family: $font_family_1;
	font-size: 14px;
	font-weight: 400;
	align-items: flex-start;
	display: flex;
	line-height: 0;
	position: relative;
	figure {

		.video-icon {
			height: 30px;
			width: 30px;
			background-color: $color_3;
			border-radius: 50%;
			bottom: 10px;
			pointer-events: none;
			position: absolute;
			right: 10px;
			&:before {
				border-width: 7px 0 7px 9px;
				border-top-color: $color_11 !important;
				border-right-color: $color_11 !important;
				border-bottom-color: $color_11 !important;
				border-left-color: $color_10;
				border-style: solid;
				content: '';
				display: block;
				height: 0;
				margin-left: 40%;
				margin-top: 30%;
				width: 0;
			}
		}
		flex-shrink: 0;
		margin: 0px;
		margin-right: 12px;
		width: 40%;
	}
	a {
		display: block;
		text-decoration: none;
	}
	.inner {
		line-height: 1.3;
	}
	&:not(.teaser-highlight) {
		.field-name-field-article-headlines a {
			color: $color_4;
		  	margin-bottom: 5px;
		}
	}
  	.field-name-field-article-headlines a{
		font-size: 10px;
		font-weight: 700;
		text-transform: uppercase;
		text-align: left;
    	padding: 0;
	}
	.field-name-node-title > a {
		font-weight: 600;
	}
	&:not(.molecule-featured) {
		.field-name-field-subtitle > a{
			display: none;
		}
	}
	[data-mod='backgroundImage'] {
    	display: none;
	}
}

.teaser-highlight {
	flex-direction: column;
	figure {
		margin: 0;
		width: 100%;
	}
	.inner {
		border-top-style: solid;
		border-top-width: 8px;
		padding: 0 12px 18px;
	}
	.field-name-node-title > a {
		font-family: $font_family_1;
		font-size: 24px;
		font-weight: 800;
	}
	.field-name-field-article-headlines{
		display: inline-block;
		position: relative;
		top: -18px;
		a {
		  padding: 8px 11px;
		  margin: 0;
		}

	}
}
.placeholder-logo {
	background: $color_6;
	&:before {
		background: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAaoAAACgCAIAAADSAb9zAAAAGXRFW…DQHwAA+gMAQH8AAOgPAAD9AQCgPwAA9AcAgP4AAIzhfwIMALnHIFCEn3NzAAAAAElFTkSuQmCC) no-repeat center;
		background-size: 50%;
		bottom: 0;
		content: '';
		display: block;
		filter: grayscale(100%);
		left: 0;
		opacity: .7;
		position: absolute;
		right: 0;
		top: 0;
	}
}
.use-image-placeholders {
	figure {
		background: $color_6;
		&:before {
			background: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAaoAAACgCAIAAADSAb9zAAAAGXRFW…DQHwAA+gMAQH8AAOgPAAD9AQCgPwAA9AcAgP4AAIzhfwIMALnHIFCEn3NzAAAAAElFTkSuQmCC) no-repeat center;
			background-size: 50%;
			bottom: 0;
			content: '';
			display: block;
			filter: grayscale(100%);
			left: 0;
			opacity: .7;
			position: absolute;
			right: 0;
			top: 0;
		}
	}
}

.molecule-featured {
	.desktop {
		display: none;
	}
	.field-name-node-title > a {
		margin: 18px 0;
	}
}
.pancake.solo {
	.video-icon {
		height: 40px;
		width: 40px;
		&:before {
			border-width: 9px 0 9px 12px;
		}
	}
}
.section-theme-border {
	border-color: $color_2 !important;
	&:before {
		border-color: $color_2 !important;
	}
}
.section-theme-inherit-border {
	* {
		&:not([class*=section-theme-]) {
			border-color: $color_2 !important;
		}
	}
}
.section-theme {
	color: $color_2 !important;
}
h1 {
	display: block;
	font-size: 2em;
	font-weight: bold;
}
[data-mod='image'] {
	background-color: $color_9;
	display: block;
	margin-top: 0;
	overflow: hidden;
	position: relative;
	width: 100%;
	img {
		position: relative;
		width: 100%;
	}
}
.comments-count {
	display: none;
	margin: 10px 0 16px;
	position: relative;
	.count-number {
		font-family: $font_family_1;
		font-size: 12px;
		font-weight: 400;
		color: $color_4;
		display: block;
		height: 14px;
		letter-spacing: 1px;
		padding: 0 0 0 22px;
		&:before {
			background: none;
			border: 0;
			content: "\f27b";
			font-family: $font_family_2;
			font-size: 16px;
			height: 16px;
			left: 0;
			position: absolute;
			top: -1px;
			width: 16px;
		}
	  	a {
		  color: $color_4;
		}
	}
}
.pancake.solo {
	&.featured {
	    .teaser-prominent {
			background: $color_1;
			text-align: center;

			.inner {
				background: linear-gradient(to bottom, rgba(20, 20, 20, 0) 0%, rgba(20, 20, 20, 1) 80px);
				border: 0;
				margin-bottom: -80px;
				padding: 60px 12px 20px;
				position: relative;
				top: -80px;
			}

		    .video-icon {
		    	top: 10px;
		    }
	    }
	}
}
.section-theme-highlight{
	background-color: $color_2 !important;
	color: $color_3;
}

.section-theme-border {
  .field-name-field-article-headlines{
	background-color: $color_2 !important;
	> a {
	  color: $color_3;
	}
  }
}

.publication-font > a, .publication-font {
	font-family: $font_family_3 !important;
	font-weight: 700;
}
.pancake.ensemble
{
	h3 {
		>a {
			font-size: 26px;
			color: $color_1;
			padding-bottom: 6px;
			text-decoration: none;
			text-transform: uppercase;

			&:after {
			    content: "\f105";
			    display: inline-block;
			    font-family: "FontAwesome";
			    font-size: 32px;
			    margin: 0 0 6px 9px;
			    vertical-align: middle;
			    color: #009C33;
			}
		}
	}
	div.teaser-highlight {
		flex-direction: column;
		.video-icon {
			height: 50px;
			width: 50px;
		  &:before {
			border-width: 11px 0 11px 15px;
		  }
		}
	  	.field-name-field-article-headlines a {
			color: $color_3;
		}
		.field-name-node-title {
			margin-left: 0;
			margin-right: 0;
		}
	}
	.teaser {
		font-size: 16px;
	}
}
.channel-sport .section-theme-border, .channel-sport .section-theme-inherit-border *:not([class*=section-theme-]), .publication-theme .channel-sport .section-theme-border, .publication-theme .channel-sport .section-theme-inherit-border *:not([class*=section-theme-]) {
    border-color: $color_7 !important;
}
.channel-sport .section-theme-highlight, .publication-theme .channel-sport .section-theme-highlight {
    background: $color_7 !important;
}
.section-theme-border, .section-theme-border:before, .section-theme-inherit-border *:not([class*=section-theme-]) {
    border-color: $color_2 !important;
}
[data-mod='mostRead'] {
    background-color: $color_3;
    counter-reset: teaser;
}
.related-column [data-mod='mostRead'], .section-page [data-mod='mostRead'] {
    display: none;
}
.base-layout [data-mod='mostRead'], .secondary [data-mod='mostRead'] {
    counter-reset: teaser;
}
.base-layout [data-mod='mostRead'] .pancake, .secondary [data-mod='mostRead'] .pancake {
    margin: 0;
    position: relative;
}
[data-mod='tabs'] {
    width: 100%;
}
[data-mod='tabs'] .tab-content {
    display: none;
    transition: display .4s ease-in-out 0s, height .2s linear 0s;
}
[data-mod='tabs'] .tab-content.active {
    display: block;
}
.base-layout [data-mod='mostRead'] .pancake .teaser, .secondary [data-mod='mostRead'] .pancake .teaser {
    background: $color_8;
    margin: 0;
    padding: 0;
    width: 100%;
}
.base-layout [data-mod='mostRead'] .pancake .teaser-highlight, .secondary [data-mod='mostRead'] .pancake .teaser-highlight {
    background: $color_9;
}
[data-mod='mostRead'] [data-value='mostRead'] .teaser {
    counter-increment: teaser;
    position: relative;
}
.placeholder-logo, .use-image-placeholders figure {
    background: $color_6;
}
.base-layout [data-mod='mostRead'] .pancake .teaser .inner, .secondary [data-mod='mostRead'] .pancake .teaser .inner {
    padding: 20px;
}
.base-layout [data-mod='mostRead'] .pancake .teaser-highlight .inner, .secondary [data-mod='mostRead'] .pancake .teaser-highlight .inner {
    padding: 0;
}
.publication-theme .section-theme-border, .publication-theme .section-theme-border:before, .publication-theme .section-theme-inherit-border *:not([class*=section-theme-]) {
    border-color: $color_2 !important;
}
[data-mod='mostRead'] [data-value='mostRead'] .teaser .inner:before {
    background: $color_10;
}
.base-layout [data-mod='mostRead'] .pancake .teaser:not(.teaser-highlight), .secondary [data-mod='mostRead'] .pancake .teaser:not(.teaser-highlight) {
    font-size: 16px;
}
.base-layout [data-mod='mostRead'] .pancake .teaser:before, .secondary [data-mod='mostRead'] .pancake .teaser:before {
    left: 20px;
    top: 20px;
}
[data-mod='mostRead'] [data-value='mostRead'] .teaser:before {
    font-size: 16px;
    color: $color_3;
    content: counter(teaser);
    display: block;
    opacity: .85;
    padding: 5px 0;
    position: absolute;
    text-align: center;
    width: 28px;
    z-index: 2;
    background: $color_10;
    line-height: 1;
}
[data-mod='mostRead'] [data-value='mostRead'] .teaser:before, .base-layout [data-mod='mostRead'] [data-value='mostRead'] .teaser-highlight .inner:before {
    font-family: 'Open Sans Condensed', sans-serif !important;
    font-weight: 700;
}
.secondary .duet figure {
	width: 52%;
}
.base-layout [data-mod='mostRead'] .pancake .teaser:not(.teaser-highlight) .field-name-node-title > a , .secondary [data-mod='mostRead'] .pancake .teaser:not(.teaser-highlight) .field-name-node-title > a {
    font-size: 14px;
}
.base-layout [data-mod='mostRead'] .pancake .teaser .field-name-field-subtitle > a, .secondary [data-mod='mostRead'] .pancake .teaser .field-name-field-subtitle > a {
    display: none;
}
.pancake[data-placeholder] {
    display: block;
}
.base-layout [data-mod='mostRead'] .pancake .teaser-highlight .field-name-field-article-headlines a, .secondary [data-mod='mostRead'] .pancake .teaser-highlight .field-name-field-article-headlines a {
    margin-left: 16px;
}
.base-layout [data-mod='mostRead'] .pancake .teaser-highlight .field-name-node-title > a, .secondary [data-mod='mostRead'] .pancake .teaser-highlight .field-name-node-title > a {
    font-size: 22px;
    padding: 0 20px 20px;
}
.base-layout [data-mod='mostRead'] [data-value='mostRead'] .teaser-highlight .field-name-node-title > a, .secondary [data-mod='mostRead'] [data-value='mostRead'] .teaser-highlight .field-name-node-title > a {
    padding-left: 66px;
}
.base-layout [data-mod='mostRead'] [data-value='mostRead'] .teaser-highlight .inner .field-name-node-title > a:before, .secondary [data-mod='mostRead'] [data-value='mostRead'] .teaser-highlight .inner .field-name-node-title > a:before {
    font-size: 26px;
    color: $color_3;
    content: counter(teaser);
    display: block;
    opacity: .85;
    padding: 5px 0;
    position: absolute;
    text-align: center;
    width: 28px;
    z-index: 2;
    line-height: 1;
	left: 20px;
}
.publication-theme .section-theme-background-indicator:before, .publication-theme .section-theme-background-indicator:after, .publication-theme .section-theme-inherit-background-indicator *:not([class*=section-theme-]):before, .publication-theme .section-theme-inherit-background-indicator *:not([class*=section-theme-]):after {
    background: $color_2 !important;
}
.section-theme-background-indicator:before, .section-theme-background-indicator:after, .section-theme-inherit-background-indicator *:not([class*=section-theme-]):before, .section-theme-inherit-background-indicator *:not([class*=section-theme-]):after {
    background: $color_2 !important;
}
.base-layout [data-mod='mostRead'] .pancake .teaser:nth-child(even), .secondary [data-mod='mostRead'] .pancake .teaser:nth-child(even) {
    background: $color_9;
}
.base-layout [data-mod='mostRead'] .pancake .teaser-highlight:before, .secondary [data-mod='mostRead'] .pancake .teaser-highlight:before {
    display: none;
}
@media (min-width: 728px) {
	.mod-pancakes {
		>* {
			margin: 0 16px;
		}
		grid-gap: 32px 16px;
	}
	.section-head {
		margin-left: 16px;
		margin-right: 16px;
		h1 {
			font-size: 40px;
		}
	}
	.tag-head {
		margin-left: 16px;
		margin-right: 16px;
		h1 {
			font-size: 40px;
		}
	}
	.topic-head {
		margin-left: 16px;
		margin-right: 16px;
		h1 {
			font-size: 40px;
		}
	}
	.pancake {
		grid-gap: 32px;
	}
	.pancake.solo {
		.video-icon {
			height: 50px;
			width: 50px;
			&:before {
				border-width: 11px 0 11px 15px;
			}
		}
	}
	.teaser-highlight {
		.field-name-node-title > a {
			font-size: 32px;
		}
	}
	.comments-count {
		display: inline-block;
	}

	.molecule-featured {
		.field-name-field-subtitle > a {
			font-size: 20px;
		}
	}
	.pancake.quartet {
		&:not(.opinion) {
			grid-template-columns: repeat(4, 1fr);
		}
		.video-icon {
			height: 40px;
			width: 40px;
			&:before {
				border-width: 9px 0 9px 12px;
			}
		}
	}
	.pancake.solo.wide {
		.teaser-prominent {
			flex-direction: row-reverse;
			.field-name-field-article-headlines > a {
				background: none !important;
			    color: $color_4;
			    padding: 0;
			    top: 0;
			}
			figure {
				width: 66%;
			}
			.inner {
				border: 0;
				padding-top: 18px;
			}
		  	.field-name-field-article-headlines {
				background: none !important;
				margin-bottom: 18px;
			  	padding: 0;
			  	top: 0;
			}
			.field-name-node-title > a {
				font-size: 24px;
			}
		}
	}
	.duet {
		grid-template-columns: repeat(2, 1fr);
		>div {
			font-size: 17px;
		}
		.video-icon {
			height: 40px;
			width: 40px;
			&:before {
				border-width: 9px 0 9px 12px;
			}
		}
	}
	.pancake.ensemble {
		h3 {
			>a {
			    font-size: 26px;
			}
		}
		div{
			.teaser-highlight {
				.video-icon {
					height: 50px;
					width: 50px;
				}
				.comments-count {
					display:block;
					margin-left: 0;
				}
			    flex-direction: column;
			}
		}
		.teaser-highlight .field-name-field-subtitle >a {
		    display: block;
		}
	}
	.teaser {
		flex-direction: column;
		&:not(.teaser-highlight) {
			figure {
				margin: 0 0 12px;
				width: 100%;
			}
			.field-name-node-title > a {
				font-weight: 700;
			}
		}
	}
	[data-mod='tabs'] .tab-content {
	    padding-top: 12px;
	}
	[data-mod='mostRead'] .pancake.ensemble {
	    margin-left: -16px;
	    margin-right: -16px;
	}
}
@media (min-width: 1024px) {
	.mod-pancakes {
		grid-template-columns: 1fr 351px;
		>* {
			grid-column: 1 / span 2;
			margin: 0 16px;
	        &.primary {
	            &+.primary+.secondary {
	              grid-row: span 2;
	            }
          	}
          	&.maximum {
		        margin: 0;
			}
		}
		>*.primary {
			grid-column: 1;
		}
		>*.secondary.pancake {
			grid-gap: 12px;
			>div {
				&:empty {	
					margin-bottom: -12px;
				}
			}
			.teaser {
				&:not(:last-of-type) {
					border-bottom: 2px solid $color_9;
					padding-bottom: 12px;
				}
			}
		}
		>*.secondary {
			border-left: 3px solid $color_9;
			grid-column: 2;
			margin-left: 0;
			padding-left: 32px;
		}
	}

	.molecule-featured {
		text-align: center;
		figure {
			&:not(.desktop) {
				display: none;
			}
		}
		.desktop {
			display: block;
		}
	}
	.pancake.quartet {
		.teaser {
			font-size: 16px;
		}
	}
	.pancake.wide {
		.teaser-prominent {
			.field-name-field-subtitle {
				display: none;
			}
		}
	}
	.duet {
		>div {
			font-size: 16px;
		}
	}
	.duet.secondary {
		grid-template-columns: 1fr;
	}
	.secondary {
		.teaser {
			flex-direction: row;
		}
	}
	.teaser.social {
		figure {
			margin: 0 12px 0 0;
			width: 52%;
		}
	  	.field-name-field-article-headlines {
			display: none;
		}
		.field-name-node-title > a {
			font-size: 13.5px;
		}
		.comments-count {
			display: none;
		}
	}
	.pancake.solo.featured .teaser-prominent {
	    flex-direction: row-reverse;
	    text-align: left;

	    figure {
	      width: 55%;
	    }

	    a {
	      margin-left: 0;
	      margin-right: 0;
	    }

	    .field-name-node-title > a {
	      font-size: 26px;
	    }

	    .field-name-field-subtitle {
	      display: block;
	      font-size: 16px;
	    }

	    .inner {
	      background: none;
	      margin: 0;
	      padding: 24px 24px 20px;
	      top: auto;
	    }

	    .field-name-field-article-headlines{
	      margin-bottom: 18px;
	      top: auto;
	    }
	    .field-name-field-subtitle a{ 
	        display: block; 
	        font-size: 16px; 
	    } 
	}
	.teaser {
	    [data-mod='backgroundImage'] {
	      bottom: 0;
	      display: block;
	      filter: blur(8px);
	      height: 100%;
	      left: 0;
	      opacity: .165;
	      position: absolute;
	      right: 0;
	      top: 0;
	    }
  	}
	.related-column [data-mod='mostRead'], .section-page .secondary [data-mod='mostRead'] {
	    display: block;
	}
	.base-layout [data-mod='mostRead'] .pancake .ensemble, .secondary [data-mod='mostRead'] .pancake .ensemble {
	    display: block;
	}
	[data-mod='mostRead'] .pancake.ensemble {
	    margin: 0;
	}
	.pancake.solo.featured .teaser-prominent {
	    flex-direction: row-reverse;
	    text-align: left;

	    figure {
	      width: 55%;
	    }

	    a {
	      margin-left: 0;
	      margin-right: 0;
	    }

	    .field-name-node-title > a {
	      font-size: 26px;
	    }

	    .field-name-field-subtitle {
	      display: block;
	      font-size: 16px;
	    }

	    .inner {
	      background: none;
	      margin: 0;
	      padding: 24px 24px 20px;
	      top: auto;
	    }

	    .field-name-field-article-headlines{
	      margin-bottom: 18px;
	      top: auto;
	    }
	}
	.teaser {
	    [data-mod='backgroundImage'] {
	      bottom: 0;
	      display: block;
	      filter: blur(8px);
	      height: 100%;
	      left: 0;
	      opacity: .165;
	      position: absolute;
	      right: 0;
	      top: 0;
	    }
  	}
  	.pancake.ensemble >.pancake {
	    border-left: 3px solid $color_9;
	    padding-left: 32px;
	    .teaser {
	    	.field-name-node-title > a {
	    		line-height: 1.4;
	    	}
	    }
	}
	.base-layout [data-mod='mostRead'] .pancake .duet, .secondary [data-mod='mostRead'] .pancake .duet {
		grid-gap: 0;
		padding: 0;
	}
	.pancake.ensemble {
	    grid-template-columns: repeat(2, 1fr);
	    h3 {
   			grid-column: span 2;
   			margin: 0px;
	    }
	    div{
	    	.teaser-highlight { 
	    		flex-direction: column;
	    	}
	    }
		.pancake.secondary {
			.teaser {
				figure {
					margin-right: 18px;
					width: 33%;
				}
			}
		}
	}
	.base-layout [data-mod='mostRead'] .pancake .teaser:not(.teaser-highlight) figure, .secondary [data-mod='mostRead'] .pancake .teaser:not(.teaser-highlight) figure {
		display: none;
	}
	.base-layout [data-mod='mostRead'] [data-value='mostRead'] .teaser:not(.teaser-highlight) .inner, .secondary [data-mod='mostRead'] [data-value='mostRead'] .teaser:not(.teaser-highlight) .inner {
		padding-left: 66px;
	}
}
@media (min-width: 1240px) {
	.mod-pancakes {
		grid-template-columns: 1fr 841px 351px 1fr;
		>* {
			grid-column: 2 / span 2;
			&.primary {
		        grid-column: 2;
		    }
		    &.secondary {
		        grid-column: 3;
		    }
			&.maximum {
		        grid-column: 1 / span 4;
		    }
		}
	}
	main {
		margin: 0 auto;
		z-index: 1;
		background-color: $color_3;
	}

	.teaser-highlight {
		.inner {
			padding: 0 24px 24px;
		}
		.field-name-node-title > a {
			font-size: 29px;
		}
	}

	.molecule-featured {
		.inner {
			box-sizing: border-box;
			padding: 0 48px 18px;
			width: 100%;
		}
		.field-name-node-title > a {
			font-size: 36px;
		    margin: 18px 0;
		}
	}
	.teaser.teaser-prominent {
		.field-name-field-subtitle {
			display: block;
		}
	}
	.teaser {
		&:not(.social) {
			&:not(.teaser-highlight) {
				.field-name-node-title > a {
					line-height: 1.5;
				}
				.field-name-field-subtitle > a {
					line-height: 1.5;
				}
			}
		}
	}
	.duet {
		>div {
			font-size: 18px;
		}
	}
	.teaser.social {
		figure {
			width: 58%;
		}
	}
	.maximum .teaser {
		overflow: hidden;
	    padding: 0 calc(50% - 620px);
	}
	.pancake.ensemble {
		h3 {
			>a {
			    font-size: 32px;
			}
		}
		.teaser-highlight{
			.comments-count {
				padding-left: 20px;
			}
		}
		div {
			.teaser-highlight {
				flex-direction: column;
			}
		}
	}
}

.channel-sport .section-theme-background-with-transparency, .publication-theme .channel-sport .section-theme-background-with-transparency {
    background: rgba(0,156,51,0.12) !important;
}

.pancake.quartet {

  @media (min-width: 728px) {

    &:not(.opinion) {
      grid-template-columns: repeat(4, 1fr);
    }
  }

  @media (min-width: 1024px) {

    &.opinion {
      grid-template-columns: repeat(2, 1fr);
    }

    .teaser {
      font-size: 16px;
    }
  }

  @media (min-width: 1240px) {
    &.opinion {
      grid-template-columns: repeat(4, 1fr);
    }
  }

}

.teaser.teaser-opinion {
  position: relative;

  *,
  *:before,
  *:after {
    box-sizing: border-box;
  }

  .author {
    @include open-sans-bold(13px);
    color: $color_3;
    display: inline-block;
    float: left;
    margin-top: 10px;
    padding: 8px 11px;
  }

  .inner {
    padding: 0;
    width: 100%;
  }

  h2 {
    float: left;
    padding: 20px 15px 15px;

    a {
      @include georgia-italic(16px);
      color: $color_12;
      float: left;
      line-height: 24px;
    }

    &:before {
      @include georgia-italic(60px);
      color: $color_12;
      content: '\2018\2018';
      float: left;
      height: 10px;
      line-height: 30px;
      position: relative;
      right: 5px;
      width: 100%;
    }
  }

  figure {
    display: none;
  }

  img {
    filter: grayscale(1);
  }

  .comments-count {
    bottom: 10px;
    display: block;
    position: absolute;
    right: 10px;
  }
}

@media (min-width: 728px) {
  .teaser.teaser-opinion {
    position: relative;

    h2 {
      float: right;
      width: calc(100% - 70px);

      a {
        @include georgia-italic(18px);
        line-height: 28px;
      }
    }

    figure {
      background-color: transparent;
      bottom: 0;
      display: inline-block;
      height: 60px;
      left: 0;
      margin: 0;
      position: absolute;
      width: 60px;

      &:before {
        display: none;
      }
    }

    .comments-count {
      right: 15px;
      top: 0;
    }
  }
}

@media (min-width: 1024px) {
  .teaser.teaser-opinion {
    h2 {
      height: 90px;
      overflow: hidden;
    }

    figure {
      height: 80px;
      width: 80px;
    }

    .inner {
      padding-bottom: 10px;
    }
  }
}

@media (min-width: 1240px) {
  .teaser.teaser-opinion {
    border-bottom: 4px solid $color_12;
    height: 310px;

    p {
      display: block;
      width: 100%;
    }

    h2 {
      height: 140px;
      overflow: hidden;
      width: 100%;
    }

    figure {
      width: 30%;
    }

    .comments-count {
      bottom: 15px;
      right: 15px;
      top: auto;
    }
  }
}

.section-head,
.tag-head,
.topic-head {
  border-bottom: 2px solid $arctic-grey;
  margin: 0 12px 24px;
}
