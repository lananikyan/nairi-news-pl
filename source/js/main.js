"use strict";

jQuery(document).ready(function ($) {
    function refreshLeft() {
        $("#top_header").removeAttr("data-expanded");
        $("#header-dropdown").removeClass("active");
    }
    
    $(window).load(function () {
        $(".loaded").fadeOut();
        $(".preloader").delay(1000).fadeOut("slow");
        refreshLeft();
    });

    $('#navbar-collapse').find('a[href*=#]:not([href=#])').click(function () {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html,body').animate({
                    scrollTop: (target.offset().top - 40)
                }, 1000);
                if ($('.navbar-toggle').css('display') != 'none') {
                    $(this).parents('.container').find(".navbar-toggle").trigger("click");
                }
                return false;
            }
        }
    });

    $(".mod-header .navigation.menu--main").on('mouseenter',".has-children", function(){
        $(this).find("[data-level='2']").addClass("open");
    });
    $(".mod-header .navigation.menu--main").on('mouseleave', ".has-children", function(){
        $(this).find("[data-level='2']").removeClass("open");
        $(this).find("[data-level='3']").removeClass("open");
        $(this).find(".has-child-group .toggle").removeClass("expanded");
    });
    $(".mod-header .navigation.menu--main").on("click", ".has-child-group .toggle", function(){
        if($(this).hasClass("expanded"))
        {
            $(this).removeClass("expanded");
            $(this).parent().find("ul").removeClass("open");
        }
        else {
            $(this).addClass("expanded");
            $(this).parent().find("ul").addClass("open");
        }
    });
    $("#hamburger").click(function(){
        if($("#top_header").attr("data-expanded") != undefined)
        {
            $("#top_header").removeAttr("data-expanded");
            $("#header-dropdown").removeClass("active");
            $("#main_body").removeClass("no-scroll");
        }
        else {
            $("#top_header").attr("data-expanded", false);
            $("#header-dropdown").addClass("active");
            $("#main_body").addClass("no-scroll");
        }
    });
    $("#header-dropdown .navigation.menu--main").on("click", ".has-children .toggle", function(){
        if($(this).parent().attr("data-children-visible") != undefined)
        {
            $(this).parent().removeAttr("data-children-visible");
            $(this).removeClass("expanded");

        }
        else {
            $(this).parent().attr("data-children-visible", true);
            $(this).addClass("expanded");
        }
    });
    $( window ).resize(function() {
        var width = $( window ).width();
        
        if(width >= 1024 )
        {
            $("#top_header").removeAttr("data-expanded");
            $("#header-dropdown").removeClass("active");
        }
        if(width >= 997)
        {
            $(".white-video-wrapper")
            .each(function(i){
                $(this).removeClass("two-column-grid").removeClass("three-column-grid").addClass("four-column-grid");
            });
        }
        if(width >= 757 && width <= 997)
        {
            $(".white-video-wrapper")
            .each(function(i){
                $(this).removeClass("two-column-grid").removeClass("four-column-grid").addClass("three-column-grid");
            });
        }
        if(width < 757)
        {
            $(".white-video-wrapper")
            .each(function(i){
                $(this).removeClass("three-column-grid").removeClass("four-column-grid").addClass("two-column-grid");
            });
        }
    });
    $('.vxp-carousel__prev-btn').click(function() {
        var left = parseInt($('.vxp-carousel__list').css("margin-left"));
        left += 548.57;
        $(this).removeClass("vxp-carousel__btn--disabled");
        if(left > 0)
        {
            left = 0;
            $(this).addClass("vxp-carousel__btn--disabled");
            $('.vxp-carousel__next-btn').removeClass("vxp-carousel__btn--disabled");
        }
        $('.vxp-carousel__list').css("margin-left",left+"px");
    });
    $('.vxp-carousel__next-btn').click(function() {
        var width = $( window ).width();
        var leftOffset = -1280;
        if( width > 768 && width <= 1278)
        {
            leftOffset = -1800+(width-768);
        }
        if( width > 1278 )
        {
            leftOffset = -1280;
        }
        var left = parseInt($('.vxp-carousel__list').css("margin-left"));
        left -= 548.57;
        $(this).removeClass("vxp-carousel__btn--disabled");
        if(left < leftOffset)
        {
            left = leftOffset;
            $(this).addClass("vxp-carousel__btn--disabled");
            $('.vxp-carousel__prev-btn').removeClass("vxp-carousel__btn--disabled");
        }
        $('.vxp-carousel__list').css("margin-left",left+"px");
    });
});